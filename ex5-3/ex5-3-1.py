# -*- coding: utf-8 -*-
if __name__ == "__main__":

    wordFreq = {}#単語をkey，出現頻度をvalueとした連想配列

    import codecs
    with codecs.open("../ex5-2/kyoto-train.voc20k.ja","r","utf-8") as inputFile:
       for line in inputFile:
           splitLine = line.strip().split(" ")
           wordFreq.update({splitLine[1]:splitLine[0]})#連想配列に格納

    for k in wordFreq.keys():#格納できているかのデバッグ用コード
        print("単語:", k, "出現回数:", wordFreq[k])

# -*- coding: utf-8 -*-
if __name__ == "__main__":

    trainWords = [] #trainに出現する単語群

    import codecs
    with codecs.open("../ex5-2/kyoto-train.voc20k.ja","r","utf-8") as inputFile:
       for line in inputFile:
           splitLine = line.strip().split(" ")
           trainWords.append(splitLine[1])

    unknownWords = [] #未知語
    
    with codecs.open("../ex5-1/kyoto-test.mrph.ja","r","utf-8") as inputFile:
        for line in inputFile:
            if not("EOS" in line):
                splitLine = line.strip().split()
                if not(splitLine[0] in trainWords):
                    unknownWords.append(splitLine[0])


    print("未知語数:",len(unknownWords),"異なり未知語数:",len(set(unknownWords)))

#出力結果
#未知語数: 1012 異なり未知語数: 763

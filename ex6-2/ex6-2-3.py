# -*- coding: utf-8 -*-
if __name__ == "__main__":

    import codecs
    import re


    neTotal = {"ARTIFACT":0, "DATE":0, "LOCATION":0, "MONEY":0, "ORGANIZATION":0, "PERCENT":0, "PERSON":0, "TIME":0}#固有表現のBとIを含めた総個数
    neCount = {"ARTIFACT":0, "DATE":0, "LOCATION":0, "MONEY":0, "ORGANIZATION":0, "PERCENT":0, "PERSON":0, "TIME":0}#固有表現のBの個数(固有表現数)
    
    with codecs.open("../ex6-1/kyoto-train.dep.ja","r","utf-8") as inputFile:
        for line in inputFile:
            neB = re.findall("B-(.+)$",line)
            neI = re.findall("I-(.+)$",line)
            if neB:
                neCount[neB[0]] = neCount[neB[0]] + 1#固有表現数のインクリメント
                neTotal[neB[0]] = neTotal[neB[0]] + 1#総個数のインクリメント
            if neI:
                neTotal[neI[0]] = neTotal[neI[0]] + 1#総個数のインクリメント

    for ne in neCount.keys():
        print(ne + "の平均形態素数:",neTotal[ne]/neCount[ne])

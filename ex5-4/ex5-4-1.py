# -*- coding: utf-8 -*-
def p(w):
    
    import codecs

    totalFreq = 0#単語総数
    oovFreq = 0#betaの分子格納用

    wordFreqTop20k = {}
    
    with codecs.open("../ex5-2/ex5-2-2output.txt","r","utf-8") as inputFile:
        for i,line in enumerate(inputFile):
            splitLine = line.strip().split()
            totalFreq = totalFreq + int(splitLine[0])
            if i < 20000:
                wordFreqTop20k.update({splitLine[1]:splitLine[0]})#Top2万語は連想配列に格納
            else:
                oovFreq = oovFreq + int(splitLine[0])#2万語以下の単語は，その出現数のみ記録

    if w in wordFreqTop20k.keys():
        #print("This word \"" + w + "\" is in Top20k")
        return int(wordFreqTop20k[w])/totalFreq #単語があればその確立を返す 
    else:
        #print("This word \"" + w + "\" is out-of-vocabulary")
        return oovFreq/totalFreq #out-of-vocabularyなのでbetaを返す
    
if __name__ == "__main__":

    w = "は"
    oov = "test"
    
    print("P:",p(w))
    print("P:",p(oov))

#This word "は" is in Top20k
#P: 0.025445906381106265
#This word "test" is out-of-vocabulary
#P: 0.04127478813580704

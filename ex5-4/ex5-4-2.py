# -*- coding: utf-8 -*-
def logLH(testWordList):
    
    import codecs,math

    totalFreq = 0#単語総数
    oovFreq = 0#betaの分子格納用

    wordFreqTop20k = {}
    
    with codecs.open("../ex5-2/ex5-2-2output.txt","r","utf-8") as inputFile:
        for i,line in enumerate(inputFile):
            splitLine = line.strip().split()
            totalFreq = totalFreq + int(splitLine[0])
            if i < 20000:
                wordFreqTop20k.update({splitLine[1]:splitLine[0]})#Top2万語は連想配列に格納
            else:
              oovFreq = oovFreq + int(splitLine[0])#2万語以下の単語は，その出現数のみ記録

    result = 0
    for w in testWordList:#テストコーパスの単語リストから対数尤度を算出する
        if w in wordFreqTop20k.keys():
            #print("This word \"" + w + "\" is in Top20k")
            result = result + math.log(int(wordFreqTop20k[w])/totalFreq) 
        else:
            #print("This word \"" + w + "\" is out-of-vocabulary")
            result = result + math.log(oovFreq/totalFreq)
    return result

if __name__ == "__main__":

    import codecs

    wordList = []
    with codecs.open("../ex5-1/kyoto-test.mrph.ja","r","utf-8") as inputFile:
        for line in inputFile:
            if not("EOS" in line):
                splitLine = line.split()
                wordList.append(splitLine[0])

    print(logLH(wordList))
